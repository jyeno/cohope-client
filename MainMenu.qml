/***************************************************************************
**
**  Copyright (C) 2020 by Jean Lima Andrade <jeno.andrade@gmail.com>
**  Copyright (C) 2020 by Vinicius Melo <vinimelo@riseup.net>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import br.edu.ifba.gsort.cohope 1.0
import "FontAwesome"

Item {
    property string title: "Cohope"

    ListView {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins/2
        clip: true
        model: [
            { icon: Icons.faDonate, text: "Quero fazer uma doação", page: "AvailableDonations.qml", donationKind: "D" },
            { icon: Icons.faBatteryQuarter, text: "Preciso de uma doação", page: "AvailableDonations.qml", donationKind: "O" },
            { icon: Icons.faUser, text: "Meu perfil", page: "UserProfile.qml", donationKind: "O", requiresAuthentication: true }
        ]
        delegate: Item {
            width: parent.width; height: frame.height
            Frame {
                id: frame
                width: parent.width
                Material.elevation: 3
                ColumnLayout {
                    width: parent.width
                    spacing: internal.margins
                    Label {
                        Layout.fillWidth: true
                        horizontalAlignment: "AlignHCenter"
                        font { family: FontAwesome.solid; styleName: "Solid"; pointSize: 22 }
                        Material.foreground: Material.Teal
                        text: modelData.icon
                    }
                    Label {
                        Layout.fillWidth: true
                        horizontalAlignment: "AlignHCenter"
                        font.capitalization: Font.AllUppercase
                        text: modelData.text
                    }
                }
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    if (modelData.requiresAuthentication === true
                        && NetworkController.context.currentUser.email === "") {
                            pageWaitingAuthentication = modelData.page
                            NetworkController.login()
                            return
                    }
                    stackView.push(modelData.page, { "donationKind": modelData.donationKind })
                }
            }
        }
    }
    Component.onCompleted: {
        if (settings.pushNotificationDonationKind !== "")
            stackView.push("AvailableDonations.qml", { "donationKind": settings.pushNotificationDonationKind })
    }
}
