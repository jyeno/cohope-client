/***************************************************************************
**
**  Copyright (C) 2020 by Jean Lima Andrade <jeno.andrade@gmail.com>
**  Copyright (C) 2020 by Vinicius Melo <vinimelo@riseup.net>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

import QtQuick 2.15
import QtQuick.Controls 2.15
import QtQuick.Controls.Material 2.15
import QtQuick.Layouts 1.15

import br.edu.ifba.gsort.cohope 1.0
import "FontAwesome"

Item {
    property var existingDonation
    property string title: ((existingDonation.kind === "O") ? "Compromissos de Recepção":"Compromissos de Doação")

    ColumnLayout {
        anchors { fill: parent; margins: internal.margins }
        spacing: internal.margins
        ListView {
            Layout.fillWidth: true; Layout.fillHeight: true
            spacing: internal.margins/2
            model: existingDonation.donation_commitments
            clip: true
            delegate: Frame {
                width: parent.width
                Material.elevation: 3
                property int commitmentIndex: index
                ColumnLayout {
                    width: parent.width
                    RowLayout {
                        Label { font { family: FontAwesome.solid; styleName: "Solid" } text: Icons.faUser; Layout.preferredWidth: 1.5*internal.margins; }
                        Label { text: ((existingDonation.kind === "D") ? "Doador: ":"Receptor: ") + (modelData.anonymous ? "anônimo":modelData.committed_user.display_name); Layout.fillWidth: true; wrapMode: Text.WordWrap }
                    }
                    Repeater {
                        model: modelData.donation_commitment_items
                        ColumnLayout {
                            property var donationItem: existingDonation.donation_items.filter((item) => { return item.id === modelData.donation_item_id; })[0]
                            Label { text: "Item: " + donationItem.description }
                            Label { text: "Quantidade: " + modelData.amount + " de " + donationItem.amount }
                        }
                    }
                }
            }
        }
    }
}
